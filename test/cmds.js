
var conf = { val: 'default' }
try { conf = require('./config') }
catch (err) { }

exports.configure = {
	file: 'conf.js', config: {
		val: 'default',
		sub: { sub: { v1: 'd1', v2: 2, a: [1, 2, 3] } }
	}
}

exports.cmds = (config) => ({
	go: 'echo "Go, go gadgets!',
	path: `echo ${process.env.PATH}`,
	myVar: `echo ${process.env.MY_VAR}`,
	myConfig: `echo ${conf.val}`,
	myConfig2: `echo '${config.val} ${config.sub.sub.a.join(' ')}'`,
	serial: ['echo 1', 'echo 2'],
	parallel: [['echo 1', 'echo 2']],
	mixed: ['echo 1', ['echo 2a', 'echo 2b'], 'echo 3'],
	env: process.platform == "win32" ? "set" : "env",
	vals: vals => {
		console.log("--- all values ---")
		console.dir(vals)
	},
	call: 'go',
	config: 'cp -n config.tmpl.js config.js',
	gen: vals => `echo ${vals.args._[0]}`,
})
