
import * as utils from "../utils"

var success = 0, failed = 0

function expect(vals) {
	return {
		to: {
			equal: (compareVals) => {
				if (vals.toString() != compareVals.toString()) {
					console.error("- '" + vals + "' != '" + compareVals + "'")
					failed++
				}
				else {
					console.log("* '" + vals + "'")
					success++
				}
			}
		}
	}
}

console.log("Run tests...")

expect(utils.parseArgs("arg")).to.equal(["arg"])
expect(utils.parseArgs("arg0 arg1")).to.equal(["arg0", "arg1"])
expect(utils.parseArgs("'arg0 arg1'")).to.equal(["arg0 arg1"])
expect(utils.parseArgs("'arg0\" arg1'")).to.equal(["arg0\" arg1"])
expect(utils.parseArgs('"arg0 arg1"')).to.equal(["arg0 arg1"])
expect(utils.parseArgs('"arg0\' arg1"')).to.equal(["arg0' arg1"])
expect(utils.parseArgs("arg0 ''")).to.equal(["arg0", ""])
expect(utils.parseArgs("arg0 '' arg1")).to.equal(["arg0", "", "arg1"])
expect(utils.parseArgs("'' arg1")).to.equal(["", "arg1"])
expect(utils.parseArgs(" arg0 ")).to.equal(["arg0"])
expect(utils.parseArgs("   arg0 ")).to.equal(["arg0"])
expect(utils.parseArgs("arg0 '\"\"' arg1")).to.equal(["arg0", '""', "arg1"])
expect(utils.parseArgs("arg0 '\"\\'\"' arg1")).to.equal(["arg0", '"\'"', "arg1"])

console.log("... " + success + " successful" + (failed ? ", " + failed + " failed!" : "") + "")
