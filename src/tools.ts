
import * as fs from "fs-extra"
import * as ncp from "ncp"
import * as path from "path"
import * as glob from "glob"
import * as rimraf from "rimraf"
import * as readline from "readline"
import * as utils from "./utils"

type DoneCb = (err?: Error) => void

export var tools = {
	copy: (vals, done: DoneCb) => copy(vals.p0, vals.p1, vals.p2, done),
	link: (vals, done: DoneCb) => fs.ensureSymlink(vals.p0, vals.p1, done),
	remove: (vals, done: DoneCb) => rimraf(vals.p0, done),
	move: (vals, done: DoneCb) => move(vals.p0, vals.p1, vals.p2, done),
	rename: (vals, done: DoneCb) => fs.rename(vals.p0, vals.p1, done),
	mkdirs: (vals, done: DoneCb) => fs.mkdirs(vals.p0, done),
	replaceText: (vals, done: DoneCb) => replaceText(vals.p0, vals.p1, vals.p2 || "",
		typeof vals.p3 === "string" ? vals.p3 : "gm", done),
	createText: (vals, done: DoneCb) => fs.writeFile(vals.p0, vals.p1, {}, done),
	prependText: (vals, done: DoneCb) => prependText(vals.p0, vals.p1, done),
	appendText: (vals, done: DoneCb) => appendText(vals.p0, vals.p1, done),
	readJson: (vals, done: DoneCb, writableVals) => readJson(vals.p0, vals.p1,
		writableVals, done),
	readPackage: (vals, done: DoneCb, writableVals) => readPackage(vals.p0, vals.p1,
		writableVals, done),
	showLines: (vals, done: DoneCb) => showLines(vals.p0, vals.p1,
		typeof vals.p2 === "string" ? vals.p2 : "", done),
	vscodeTasks: (vals, done: DoneCb) => vscodeTasks(vals, done),
	npmScripts: (vals, done: DoneCb) => npmScripts(vals, done),
}

function processFiles(base: string, filesGlob: string, destPath: string,
	fn, opts, msg: string, done) {
	if (!destPath) {
		destPath = filesGlob
		filesGlob = base
		base = null
	}
	if (base == ".") base = null
	msg = `${filesGlob} => ${msg} `
	if (base) {
		msg = `${base} ${msg}`
		filesGlob = path.join(base, filesGlob)
	}
	glob(filesGlob, { cwd: ".", nodir: false }, function(err, files) {
		if (err) return done(err)
		let fileCount = files.length
		if (fileCount != 1 || files[0] != filesGlob) console.log(msg + fileCount + " files...")
		files.forEach(function(f) {
			const d = base ? f.replace(base, destPath) : path.join(destPath, f)
			fs.mkdirs(path.dirname(d), err => {
				if (err) return done(err)
				fn(f, d, opts, (err) => {
					if (err) return done(err)
					fileCount--
					if (fileCount <= 0) done()
				})
			})
		})
	})
}

const moveOptions = { clobber: false, mkdirp: true }
const copyOptions = { clobber: true, perserveTimestamps: true }

export function copy(base: string, filesGlob: string, destPath: string, done) {
	processFiles(base, filesGlob, destPath, ncp.ncp, copyOptions, "copying", done)
}

export function move(base: string, filesGlob: string, destPath: string, done) {
	processFiles(base, filesGlob, destPath, fs.move, moveOptions, "moving", done)
}

// TODO: factor out common content toolling

export function replaceText(filesGlob: string, regExp: string | RegExp, replaceValue: string,
	flags: string, done: { (err?: Error): void }) {
	glob(filesGlob, { cwd: "." }, function(err, files) {
		if (err) return done(err)
		var regEx = typeof regExp === "string" ? new RegExp(regExp, flags) : regExp
		var fileCount = files.length
		if (fileCount != 1 || files[0] != filesGlob)
			console.log(filesGlob + " => replacing content in " + fileCount + " files...")
		files.forEach(function(f) {
			console.log(f)
			fs.readFile(f, (err, data) => {
				if (err) return done(err)
				var newContent = data.toString().replace(regEx, replaceValue)
				fs.writeFile(f, newContent, {}, err => {
					if (err) return done(err)
					fileCount--
					if (fileCount <= 0) done()
				})
			})
		})
	})
}

export function showLines(filesGlob: string, filterRegExp: string | RegExp, flags: string, done) {
	glob(filesGlob, { cwd: "." }, function(err, files) {
		if (err) return done(err)
		var regEx = typeof filterRegExp === "string" ? new RegExp(filterRegExp, flags) : filterRegExp
		var fileCount = files.length
		files.forEach(function(f) {
			const rl = readline.createInterface({ input: fs.createReadStream(f), terminal: false })
			rl.on('line', (line) => { if (!regEx || regEx.test(line)) console.log(line) })
			rl.on('close', () => {
				if (err) return done(err)
				fileCount--
				if (fileCount <= 0) done()
			})
		})
	})
}

export function prependText(filesGlob: string, content: string,
	done: (err?: Error) => void) {
	glob(filesGlob, { cwd: "." }, function(err, files) {
		if (err) return done(err)
		var fileCount = files.length
		if (fileCount != 1 || files[0] != filesGlob)
			console.log(filesGlob + " => prepending content in " + fileCount + " files...")
		files.forEach(function(f) {
			console.log(f)
			fs.readFile(f, (err, data) => {
				if (err) return done(err)
				// TODO: directly via buffer?
				var newContent = content + data.toString()
				fs.writeFile(f, newContent, {}, err => {
					if (err) return done(err)
					fileCount--
					if (fileCount <= 0) done()
				})
			})
		})
	})
}

export function appendText(filesGlob: string, content: string,
	done: (err?: Error) => void) {
	glob(filesGlob, { cwd: "." }, function(err, files) {
		if (err) return done(err)
		var fileCount = files.length
		if (fileCount != 1 || files[0] != filesGlob)
			console.log(filesGlob + " => appending content in " + fileCount + " files...")
		files.forEach(function(f) {
			console.log(f)
			fs.readFile(f, (err, data) => {
				if (err) return done(err)
				// TODO: directly via buffer?
				var newContent = data.toString() + content
				fs.writeFile(f, newContent, {}, err => {
					if (err) return done(err)
					fileCount--
					if (fileCount <= 0) done()
				})
			})
		})
	})
}

export function readJson(fileName: string, valKey: string, vals, done) {
	fs.readFile(fileName, (err, data) => {
		if (err) return done(err)
		try {
			vals[valKey] = JSON.parse(data.toString())
			done()
		}
		catch (err) { done(err) }
	})
}

export function readPackage(fileName: string, valKey: string, vals, done) {
	fs.readFile(fileName, (err, data) => {
		if (err) return done(err)
		try {
			vals[valKey] = utils.formatPackage(JSON.parse(data.toString()))
			done()
		}
		catch (err) { done(err) }
	})
}

export function vscodeTasks(vals, done: (err?: Error) => void) {
	var vsPath = vals.args.vscodePath || ".vscode"
	fs.mkdirsSync(vsPath)
	var tasksFile = path.join(vsPath, "tasks.json")
	var ncmdBin = "node_modules/ncmd/bin/ncmd"
	fs.readFile(tasksFile, (err, data) => {
		// expect comments within JSON
		var old: any = {}
		if (!err) eval("old = " + data)
		var json = old
		json.command = "node"
		json.isShellCommand = true
		var hasNcmdArg
		if (!json.tasks) json.tasks = []
		var tasks = {}
		if ("node" == old.command && old.tasks.length > 0) {
			// reuse valid old tasks and keep args
			for (var i = 0, t; (t = old.tasks[i]); ++i) tasks[t.taskName] = t
			if (!json.args || json.args.length < 1) {
				hasNcmdArg = false
			}
			else {
				var scriptArgs = json.args.filter((arg) => arg.charAt(0) != '-')
				if (scriptArgs.length == 1 && /ncmd$/.test(scriptArgs[0])) {
					hasNcmdArg = true
				}
				else if (scriptArgs.length < 1) {
					hasNcmdArg = false
				}
				else {
					// multiple node scripts are not supported here...
					json.args = [ncmdBin]
					hasNcmdArg = true
					json.tasks = []
					tasks = {}
				}
			}
		}
		else {
			json.args = [ncmdBin]
			hasNcmdArg = true
		}
		var specs = utils.flatten(vals.cmds, {})
		for (var k in specs) {
			if (vals.args.exclude && k.search(vals.args.exclude) >= 0) continue
			if (tasks[k]) continue
			var tsk = { "taskName": k, args: [] }
			if (!hasNcmdArg) tsk.args.push(ncmdBin)
			if ("ncmd.vscodeTasks" == k) tsk.args.push("--exclude", "^_|\\._")
			if ("ncmd.npmScripts" == k) tsk.args.push("--exclude", "^_|\\._")
			if (tsk.args.length < 1) delete tsk.args
			json.tasks.push(tsk)
		}
		if (old.command != json.command
			|| (old.args && old.args.length > 0 && old.args[0] != json.args[0])) {
			// stuff will be overwritten => save backup
			var f = "tasks_" + new Date().toISOString().replace(/:/g, '_') + ".json"
			fs.writeFile(path.join(vsPath, f), data, {}, err => {
				if (err)
					// ignore for now...
					console.log(err.message)
			})
		}
		fs.writeFile(tasksFile, JSON.stringify(json, null, '\t'), {}, done)
	})
}

export function npmScripts(vals, done: (err?: Error) => void) {
	var json = { ...vals.pkg, versionMajor: void 0, versionMinor: void 0 }
	if (!json.scripts) json.scripts = {}
	var specs = utils.flatten(vals.cmds, {})
	for (var k in specs) {
		if (vals.args.exclude && k.search(vals.args.exclude) >= 0) continue
		if (json.scripts[k]) continue
		json.scripts[k] = "ncmd " + k
	}
	fs.writeFile("package.json", JSON.stringify(json, null, '\t'), {}, done)
}
