
import * as fs from "fs"

export function requireFirst(paths: string[], verbose = true) {
	for (var i = 0, path: string; (path = paths[i]); ++i) {
		try { return { path, content: require(path) } }
		catch (err) {
			if (err.code !== "MODULE_NOT_FOUND") throw err
			else if (verbose) console.error(err.message)
		}
	}
	return null
}

export function flatten(obj, objs, parentKey?: string) {
	if (obj === void 0 || obj === null) return objs
	Object.keys(obj).forEach(function(k) {
		var v = obj[k]
		var key = parentKey ? parentKey + '.' + k : k
		var t = typeof v
		if (t === 'string' || t === 'function' || Array.isArray(v)) {
			objs[key] = v
			if (k === 'default' && parentKey) objs[parentKey] = v
		}
		else flatten(v, objs, key)
	})
	return objs
}

export function formatObj(obj: any, vals: any) {
	if (typeof obj === "string") return format(obj, vals)
	var res = {}
	for (var k in obj) res[k] = formatObj(obj[k], vals)
	return res
}

export function format(str: string, vals: any, removeUnknown = false) {
	return str.replace(/\{([a-z_][^}]*)}/g, function(match, key: string) {
		var keys = key.split('.'), v = vals
		for (var i = 0, k; (k = keys[i]) && v; ++i) v = v[k]
		return v || v == 0 ? v : removeUnknown ? "" : "{" + key + "}"
	})
}

export function formatPackage(pkg) {
	return pkg && pkg.version ? {
		...pkg,
		// version parts
		versionMinor: pkg.version.substring(0, pkg.version.lastIndexOf('.')),
		versionMajor: pkg.version.substring(0, pkg.version.indexOf('.')),
	} : pkg
}

export function extendEnvPath(env, ...paths: string[]) {
	var PATH = "PATH"
	// windows calls it's path "Path" usually, but this is not guaranteed.
	if (process.platform === "win32") {
		PATH = "Path"
		Object.keys(env).forEach((e) => { if (e.match(/^PATH$/i)) PATH = e })
	}
	var separator = process.platform === "win32" ? ";" : ":"
	var path = env[PATH] + separator || separator
	for (let p of paths) {
		if (path.indexOf(p + separator) < 0) path += p + separator
	}
	env[PATH] = path.substr(0, path.length - 1)
	return env
}

export function fileStats(path: string): fs.Stats {
	try { return fs.statSync(path) }
	catch (err) {
		if (err.code !== "ENOENT") throw err
	}
}

export function parseArgs(argStr: string) {
	var q = null, s = 0, args: string[] = []
	for (var i = 0; i < argStr.length; ++i) {
		var c = argStr.charAt(i)
		if (q && c != q) continue
		else if (s == i && (c == ' ')) s++
		else if (c == ' ' || (q && c == q)) {
			args.push(argStr.substring(s, i))
			s = i + 1
			q = null
		}
		else if (s == i && (c == '"' || c == "'")) {
			q = c
			s = i + 1
		}
	}
	if (s < argStr.length && argStr.length > 0) args.push(argStr.substring(s))
	return args
}

/** Mask an object with the members of another object (mask).
 * Array values of the mask replace all the values of the object member. 
 */
export function mask<T>(obj: T, maskObj): T {
	if (maskObj === void 0 || maskObj === null)
		return obj
	if (obj === void 0 || obj === null)
		return maskObj
	if (Array.isArray(obj))
		return Array.isArray(maskObj) ? [...maskObj] : [maskObj] as any
	if (typeof obj === 'object') {
		const res = {} as T
		for (const k of Object.keys(obj)) {
			const v = obj[k]
			if (k in maskObj) {
				const m = maskObj[k]
				res[k] = mask(v, m)
			}
			else
				res[k] = v
		}
		for (const k of Object.keys(maskObj)) {
			if (!(k in obj)) res[k] = maskObj[k]
		}
		return res
	}
	return maskObj
}
