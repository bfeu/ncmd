/// <reference path="ext.ts" />

import proc = require('child_process')
import os = require('os')
import path = require('path')
import fs = require('fs')
import parseArgs = require("minimist")
import ansi = require("./ansi")
import utils = require("./utils")
import { tools } from "./tools"
import * as glob from "glob"
import { register } from 'ts-node'
import { config } from 'dotenv'

config()

var ncmdPkg = require('../package.json')

var usage = `Node Commands ${ncmdPkg.version}
Usage: ncmd <command> [-s] [-v] [-p path/to/package.json] [-c path/to/ncmd.js] [-C current/working/directory]

 --pkg [-p, --package]
  Path to npm package.json relative to cwd. Defaults to './package.json'
 
 --cmds [-c, --commands]
  Path to cmds.ts/js config script relative to cwd. Defaults to './cmds.ts' or './build/cmds.ts'.
	When no cmds.js is found and a cmds.ts is specified/found, it will automatically be compiled first.
	
 --force [-f]
	Force a prepare command to be executed.
	Eg. compile cmds.ts, even if the cmds.js seems up-to-date. Useful to compile changes to
	files imported by cmds.ts.
 
 --cwd [-C]
  Working directory for to run the commands in. Defaults to '.'

 --silent [-s]
  Less output.
 
 --verbose [-v]
  More detailed output.

 --tsconfig
  Path to explicit tsconfig.json to compile the cmds.ts input file. Defaults to internal config
	and cwd/tsconfig.json is ignored (is likely to be project specific).
 
 --version
  Version of this app.
 
 --help [-h]
  Output this help message.
 
Commands not found in config will be executed directly (after string substitution).`

interface Args {
	pkg: string,
	cmds: string,
	force: boolean,
	cwd: string,
	tsconfig: string,
	silent: boolean,
	verbose: boolean,
	version: boolean,
	help: boolean,
	_: string[]
}

var args = <Args><any>parseArgs(process.argv.slice(2), {
	default: {
		pkg: "./package.json",
		cmds: "**/*/cmds.ts",
		cwd: null,
		tsconfig: require.resolve('./tsconfig.json')
	},
	alias: {
		p: "pkg",
		package: "pkg",
		c: "cmds",
		commands: "cmds",
		f: "force",
		C: "cwd",
		s: "silent",
		v: "version",
		h: "help"
	},
	boolean: ["silent", "s", "verbose", "v", 'version', "help", "h"],
	string: ['pkg', 'cmds', 'cwd', 'tsconfig'],
})
if (args.help) {
	console.log(usage)
	process.exit(0)
}

if (args.version) {
	console.log(`Node Commands ${ncmdPkg.version}`)
	process.exit(0)
}

// home directory in Windows
const homeRegex = /^~/
const formatHomeDir = p => p && process.platform === "win32"
	? p.replace(homeRegex, process.env.USERPROFILE) : p
if (process.platform === "win32") {
	args.pkg = formatHomeDir(args.pkg)
	args.cmds = formatHomeDir(args.cmds)
	args.cwd = formatHomeDir(args.cwd)
}

// working directory
var cwd
if (args.cwd && args.cwd != ".") {
	console.log("Change to " + args.cwd)
	cwd = process.cwd()
	process.chdir(args.cwd)
}

// register Typescript compiler for cmds.ts files
register({ project: path.resolve(args.tsconfig) })

// global info
var globalVals = {
	pkg: utils.formatPackage(utils.requireFirst(['.', '..']
		.map(p => path.resolve(p, args.pkg)), args.verbose).content),
	env: Object.assign({}, process.env),
	os: {
		homedir: os.homedir(), tmpdir: os.tmpdir(), hostname: os.hostname(), type: os.type(),
		platform: os.platform()
	},
	args: args,
}

function extendPath(env) {
	if (utils.fileStats("./node_modules/.bin"))
		env = utils.extendEnvPath(env, path.resolve(path.join("node_modules", ".bin")))
	if (utils.fileStats("./build/node_modules/.bin"))
		env = utils.extendEnvPath(env, path.resolve(path.join("build", "node_modules", ".bin")))
	return env
}
extendPath(globalVals.env)

type CompleteCb = (success: boolean) => void
type CmdFunction = ((vals?, done?: (err?) => void, writableVals?) => void) |
	((vals?) => string | void)

class OneTimeCb {
	constructor(public cb: CompleteCb) { }
	go(success: boolean) {
		if (!this.cb) return
		this.cb(success)
		delete this.cb
	}
}

class CmdExecStatus {
	runningCmds: { [key: string]: CompleteCb[] } = {}
	completedCmds: { [key: string]: boolean } = {}
}

class Cmd {
	static specs = {}
	static vals = {}
	static idCount = 0
	id = ++Cmd.idCount
	next: Cmd
	nextOnError: Cmd

	constructor(public cmd: string | CmdFunction | Cmd | Cmd[], public level = 0,
		private _name?: string, private _params?: string[], public parent?: Cmd) {
		if (this.parent) {
			let p = this.parent
			while (p) {
				if (p.cmd === this.cmd) {
					// circular definition => ignore further
					console.error("Circular definitions are not supported!", this._name)
					delete this.cmd
					return
				}
				p = p.parent
			}
		}
		if (Array.isArray(cmd)) {
			if (!this._name) this._name = 'async_' + this.id
			for (var i = 0, c; (c = cmd[i]); ++i)
				this.cmd[i] = Cmd.build(c, level + 1, void 0, this._params, this)
		}
		else if (typeof cmd === "string") {
			// it might be a reference to another command
			const paramIdx = cmd.indexOf(" ")
			const ref = paramIdx > 0 ? cmd.substring(0, paramIdx) : cmd
			const c = Cmd.specs[ref]
			if (c && c != cmd) {
				const _params = paramIdx > 0 ? utils.parseArgs(cmd.substring(paramIdx + 1)) : void 0
				this.cmd = Cmd.build(c, level + 1, cmd, _params, this)
				if (!this._name) this._name = "ref_" + this.id + "->" + cmd
				// there might be an on-error command
				const n = ref + "_error"
				const onErrCmd = Cmd.specs[n]
				if (onErrCmd)
					this.nextOnError = Cmd.build(onErrCmd, this.level + 1, n, _params, this)
			}
			else if (!this._name) {
				this._name = "sync_" + this.id
			}
		}
	}

	static build(cmds, level = 0, name?: string, params?: string[], parent?: Cmd) {
		if (Array.isArray(cmds)) {
			var startCmd = new Cmd(cmds[0], level, void 0, params, parent)
			var cmd = startCmd
			for (var i = 1, cmdV; (cmdV = cmds[i]); ++i) {
				cmd = cmd.next = new Cmd(cmdV, level, void 0, params, parent)
			}
			return startCmd
		}
		return new Cmd(cmds, level, name, params, parent)
	}

	private complete(success, complete: OneTimeCb, status: CmdExecStatus,
		cwd?: string, stop = false) {
		this.log("..." + this._name + (!success ? " failed!" : ""))
		var runningCbs = status.runningCmds[this.id]
		if (runningCbs) {
			for (var i = 0, cb: CompleteCb; (cb = runningCbs[i]); ++i) cb(success)
			delete status.runningCmds[this.id]
		}
		status.completedCmds[this.id] = success
		//console.log(complete.cb)
		if (success && this.next && !stop) this.next.exec(complete, cwd, status)
		else if (!success && this.nextOnError && !stop) this.nextOnError.exec(complete, cwd, status)
		else complete.go(success)
	}

	private log(msg, level = 0) {
		if (args.silent) return
		if (!args.verbose && level < 1)
			return
		if (args.verbose) {
			msg = ansi.color.green + this.id + ": " + ansi.color.cyan + msg
			for (var i = 0; i < this.level + 1; ++i) msg = "  " + msg
		}
		else {
			msg = ansi.color.gray + "# " + msg
		}
		console.info(msg + ansi.color.off)
	}

	private static setParams(obj, params, fVals) {
		if (!params || params.length <= 0) return obj
		const vals: string[] = []
		for (let i = 0; i < params.length; ++i)
			vals.push(utils.format(params[i], fVals))
		for (let i = 0; i < params.length; ++i) {
			obj["p" + i] = vals[i]
			obj["p" + i + "_"] = vals.slice(i).join(" ")
		}
		obj.p_ = obj.p0
		obj["p" + params.length + "_"] = ""
		return obj
	}

	getFormattedVals() {
		const fVals = Object.assign({}, Cmd.vals)
		return Cmd.setParams(fVals, this._params, this.parent ? this.parent.getFormattedVals() : fVals)
	}

	exec(completeCb: CompleteCb | OneTimeCb, cwd?: string, status = new CmdExecStatus()) {
		this.log(this._name + "...")
		var cb = typeof completeCb === "function"
			? new OneTimeCb(<CompleteCb>completeCb)
			: <OneTimeCb>completeCb
		var completedSuccess = status.completedCmds[this.id]
		if (typeof completedSuccess === 'boolean') {
			this.complete(completedSuccess, cb, status, cwd)
			return
		}
		var running = status.runningCmds[this.id]
		if (running) {
			running.push((success) => this.complete(success, cb, status, cwd))
			return
		}
		status.runningCmds[this.id] = []
		if (Array.isArray(this.cmd)) {
			// cmdLn arrays are executed asynchronously
			var pending = (<Cmd[]>this.cmd).length
			for (var c = 0, cmd; (cmd = this.cmd[c]); ++c)
				cmd.exec((success) => {
					if (success) {
						pending--
						if (pending > 0) return
					}
					this.complete(success, cb, status, cwd)
				}, cwd)
		}
		else if (typeof this.cmd === "function") {
			// direct function calls
			// TODO: provide cwd
			if ((<CmdFunction>this.cmd).length > 1) {
				void (<CmdFunction>this.cmd)(this.getFormattedVals(), (err) => {
					if (err) console.error(err.message || err)
					this.complete(!err, cb, status, cwd)
				}, Cmd.vals)
			}
			else {
				const res = (<CmdFunction>this.cmd)(this.getFormattedVals())
				if (typeof res === 'string') {
					const next = new Cmd(res, this.level + 1, this._name + '.return', this._params, this)
					next.exec((success) => this.complete(success, cb, status, cwd), cwd)
				}
				else {
					this.complete(true, cb, status, cwd)
				}
			}
		}
		else if (typeof this.cmd === "string" && (<string>this.cmd).indexOf("cd ") === 0) {
			// change working directory
			var d = utils.format((<string>this.cmd).substring(3), this.getFormattedVals(), true)
			this.complete(true, cb, status, path.join(cwd ? cwd : ".", formatHomeDir(d)))
		}
		else if (typeof this.cmd === "string" && (<string>this.cmd).indexOf("if ") === 0) {
			// test condition
			let stop = false
			// quick, dirty & minimal impl...
			const tks = (<string>this.cmd).split(' ')
			if (tks.length >= 3) {
				const not = tks[1] === 'not'
				const test = tks[not ? 2 : 1]
				let arg = formatHomeDir(utils.format(tks[not ? 3 : 2], this.getFormattedVals(), true))
				// TODO: test this...
				if (cwd) path.join(cwd, arg)
				try {
					if (test === 'isDir') stop = !fs.lstatSync(arg).isDirectory()
					else if (test === 'isFile') stop = !fs.lstatSync(arg).isFile()
					else if (test === 'exists') fs.lstatSync(arg)
				} catch (err) { stop = true }
				if (not) stop = !stop
			}
			this.complete(true, cb, status, cwd, stop)
		}
		else if (typeof this.cmd === "string" && (<string>this.cmd).indexOf("# ") === 0) {
			// simple log
			var msg = utils.format((<string>this.cmd).substring(2), this.getFormattedVals(), true)
			this.log(ansi.color.gray + msg, 1)
			this.complete(true, cb, status, cwd)
		}
		else if (typeof this.cmd === "string") {
			// command line
			var cmdLn = utils.format(<string>this.cmd, this.getFormattedVals(), true)
			this.log(ansi.color.gray + cmdLn, 1)
			// per command directory
			let dir = cwd
			const dirSpec = cmdLn.match(/^([\w\/\.-]+|"[^"]+"): /)
			if (dirSpec && dirSpec.length > 1) {
				dir = dirSpec[1]
				cmdLn = cmdLn.substring(dir.length + 2)
				if (dir.charAt(0) === '"') dir = dir.substring(1, dir.length - 1)
				if (cwd) dir = path.join(cwd, dir)
				if (!fs.existsSync(dir)) {
					console.error(ansi.color.lightred + `Path '${dir}' not found!` + ansi.color.off)
					this.complete(false, cb, status, cwd)
					return
				}
			}
			// from https://github.com/nodejs/node/blob/master/lib/child_process.js
			var isWin = process.platform === "win32"
			var shell = isWin ? process.env.comspec || "cmd.exe" : "/bin/sh"
			var args = isWin ? ["/s", "/c", '"' + cmdLn + '"'] : ["-c", cmdLn]
			var opts: any = {
				env: globalVals.env,
				stdio: "inherit",
			}
			if (dir) {
				opts.cwd = dir
				// include possible node module executables into the path
				const nodeBin = path.join(dir, 'node_modules', '.bin')
				if (utils.fileStats(nodeBin))
					opts.env = utils.extendEnvPath({ ...globalVals.env }, path.resolve(nodeBin))
			}
			if (isWin) opts.windowsVerbatimArguments = true
			var p = proc.spawn(shell, args, opts)
			var reportErr = (msg) =>
				console.error(ansi.color.lightred +
					"Command failed: " + shell + " " + args.join(" ") + (msg ? "\n" + msg : "") +
					ansi.color.off)
			p.on("close", (code) => {
				if (code != 0) reportErr("Exit code: " + code)
				this.complete(code == 0, cb, status, cwd)
			})
			p.on("error", (err) => {
				reportErr(err ? err.message : null)
				this.complete(false, cb, status, cwd)
			})
		}
		else if (this.cmd) {
			// the first command of a sequence
			(<Cmd>this.cmd).exec((success) => this.complete(success, cb, status, cwd), cwd)
		}
		else {
			// it has been removed
			this.complete(true, cb, status, cwd)
		}
	}

}

function addVals(obj, vals, type = "Value") {
	for (var k in vals) {
		if (!vals.hasOwnProperty(k)) continue
		if (obj.hasOwnProperty(k))
			console.warn(ansi.color.yellow + `${type} '${k}' is hidden!` + ansi.color.off)
		obj[k] = vals[k]
	}
	return obj
}

function onComplete(success) {
	if (!args.silent) {
		if (success) console.log(ansi.color.cyan + "finished." + ansi.color.off)
		else console.error("failed!")
	}
	// change back into the original working directory
	if (cwd) process.chdir(cwd)

	process.exit(success ? 0 : 1)
}

function readCmds(cmdsFile: string) {
	const cmdDef = {
		file: cmdsFile, vals: Object.assign({ tools: tools, cmds: {} }, globalVals),
		specs: null, cwd: path.dirname(cmdsFile)
	}
	// prepare vals and cmds
	const mod = utils.requireFirst(['.', '..'].map(p => path.resolve(p, cmdsFile)))
	let cmds = mod ? mod.content : {}
	if (cmds.cmds) {
		// separate exports
		addVals(cmdDef.vals, cmds.vals)
		addVals(cmdDef.vals.tools, cmds.tools, "Tool")
		if (cmds.cwd) cmdDef.cwd = path.resolve(cmdDef.cwd, cmds.cwd)
		const config = cmds.configure ? configure(mod.path, cmds.configure) : {}
		cmds = typeof cmds.cmds === 'function' ? cmds.cmds(config) : cmds.cmds
	}
	else {
		// one default export with key sections
		if (cmds._vals) {
			addVals(cmdDef.vals, cmds._vals)
			delete cmds._vals
		}
		if (cmds._tools) {
			addVals(cmdDef.vals.tools, cmds._tools, "Tool")
			delete cmds._tools
		}
	}
	cmdDef.vals.cmds = cmds
	cmdDef.specs = utils.flatten(addVals(Object.assign({}, tools), cmds, "Tool"), {})
	for (var k in cmdDef.vals)
		if (k !== "pkg" && k !== "args" && k !== "env" && k !== "cmds")
			cmdDef.vals[k] = utils.formatObj(cmdDef.vals[k], cmdDef.vals)
	return cmdDef
}

function findLocalCmd(files, cmd) {
	for (let f of files) {
		if (utils.fileStats(f)) {
			const defs = readCmds(f)
			if (cmd in defs.specs) return defs
		}
	}
}

function findCmd(files, cmd) {
	for (let f of files) {
		const defs = readCmds(f)
		if (cmd in defs.specs) return defs
	}
}

function findGlobalCmd(cmd) {
	const home = os.homedir()
	for (let f of ['cmds.ts', 'cmds.js']) {
		f = path.join(home, f)
		if (utils.fileStats(f)) {
			const defs = readCmds(f)
			if (cmd in defs.specs) return defs
		}
	}
}

function start(cmdArg, def) {
	const cmd = def.specs[cmdArg]
	Cmd.specs = def.specs
	Cmd.vals = def.vals
	const startCmd = Cmd.build(cmd, 0, cmdArg, args._.slice(1))
	// there might be an on-error command
	const n = cmdArg + "_error"
	const onErrCmd = Cmd.specs[n]
	if (onErrCmd)
		startCmd.nextOnError = Cmd.build(onErrCmd, 0, n, args._.slice(1))
	if (def.cwd && def.cwd != '.' && !args.cwd)
		process.chdir(def.cwd)
	extendPath(def.vals.env)
	console.log('Cmd ' + cmdArg + ' in ' + def.file + ', running at ' + process.cwd())
	startCmd.exec(onComplete)
}

// TODO: run multiple commands
const cmdArg = args._[0] || "default"
const def = findLocalCmd(['cmds.ts', 'cmds.js',
	'build/cmds.ts', 'build/cmds.js',
	'test/cmds.ts', 'test/cmds.js',
	'lib/cmds.ts', 'lib/cmds.js',
	'app/cmds.ts', 'app/cmds.js',
], cmdArg)
if (cmdArg in tools) {
	// run tools directly (no need for a cmds.js)
	Cmd.specs = [cmdArg]
	Cmd.vals = def ? def.vals : globalVals
	const cmd = Cmd.build(tools[cmdArg], 0, cmdArg, args._.slice(1))
	cmd.exec(onComplete)
} else {
	if (def) {
		start(cmdArg, def)
	} else {
		try {
			const files = glob.sync(args.cmds, {
				cwd: ".", nodir: true, nosort: true, ignore: '**/node_modules/**', dot: false,
			})
			const count = (ch, str) => {
				let c = 0
				for (let i = 0; i < str.length; ++i) if (str.charAt(i) === ch) c++
				return c
			}
			const comp = (a: string, b: string) => {
				const ac = count('/', a), bc = count('/', b)
				return ac > bc || (ac === bc && a > b) ? 1 : -1
			}
			const def = findCmd(files.sort(comp), cmdArg) || findGlobalCmd(cmdArg)
			if (def) {
				// configured command
				start(cmdArg, def)
			}
			else {
				if (args._.length <= 0) {
					console.error(
						"\nPlease specify a command to run or define a 'default' command in your script!\n")
					console.log(usage)
					process.exit(1)
				}
				// unknown command => direct command line
				Cmd.build(process.argv.slice(2).join(' ')).exec(onComplete)
			}
		}
		catch (err) {
			console.error(err)
			process.exit(2)
		}
	}
}


function configure(basePath: string,
	{ config, file }: { config: any, file: string }) {
	const f = path.resolve(path.join(basePath, '..'), file);
	if (fs.existsSync(f)) {
		return utils.mask(config, require(f));
	}
	else {
		const prefix = f.endsWith('.ts') ? 'export const ' : 'exports.'
		const content = Object.keys(config)
			.map(k => `${prefix}${k} = ${stringify(config[k])}`)
			.join('\n')
		fs.writeFile(f, content, err => { err && console.error(err); });
		return config;
	}
}

function stringify(obj: any) {
	const str = JSON.stringify(obj, null, '\t')
	return str.indexOf("'") < 0 ? str.replace(/"/g, "'") : str
}
