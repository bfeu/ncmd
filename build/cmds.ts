
import sh = require("shelljs")

import * as imp from "./import"
import { cmds as im } from "./import"

export var vals = {
	v1: "v1 test",
	v2: { v21: "v2.1 test" },
	v0: imp.vals,
	v3: "v {pkg.name}",
	v4: "v1: {v1}"
}

export const configure = { config: { val: 'default' }, file: 'config.js' }

export const cmds = (conf: (typeof configure)['config']) => ({
	testConf: `echo ${conf.val}`,
	...im,
	build: ["tsc -p ../src/", "remove ../bin/ncmd",
		"rename ../bin/ncmd.js ../bin/ncmd",
		"replaceText ../bin/ncmd '/// <reference path=.*/>'",
		"prependText ../bin/ncmd '#!/usr/bin/env node\n'"],
	default: "vals",
	build_js: "echo \"build_js\"",
	build_css: ["echo \"build_css1\" && sleepms 200", "echo \"build_css2\" && sleepms 100"],
	release: {
		build: ["echo \"prep release build\" && sleepms 200", ["release.build_css", "release.build_js"]],
		build_js: "echo \"release.build_js\" && sleepms 300",
		build_css: ["build_css"
			, ["echo \"a1\" && sleepms 200", "echo \"a2\" && sleepms 100"]
			, "echo \"release.build_css\" && sleepms 300"
			, ["echo \"a11\" && sleepms 200", "echo \"a12\" && sleepms 100"]
		],
	},
	pull: "git pull",
	platform: "echo " + process.platform,
	env: process.platform == "win32" ? "set" : "env",
	vals: (vals) => {
		console.log("--- all values ---")
		console.dir(vals)
		console.log("---")
		console.log("The are offered as an argument for functions like this" +
			" and for cross-platform string substitution {key.key...}.")
		console.log("---")
	},
	valSubst: "echo \"pkg.name: {pkg.name}, v1: {v1}, v21: {v2.v21}\"",
	cmdSubst: "echo \"{cmds.pull}\"",
	play: (vals) => {
		console.log("--- play in " + sh.pwd() + " ---")
		sh.mkdir("-p", "tmp_play/test1")
		sh.cp("-f", "*.md", "tmp_play/test1")
			; (<any>"test end").to("tmp_play/testEnd.txt")
			; (<any>sh.cat("package.json", "tmp_play/test1/readme.md", "tmp_play/testEnd.txt")).to("tmp_play/out.txt")
		sh.rm("-Rf", "tmp_play")
	},
	parameters: {
		base: [["echo \"p0: {p0}, p1: {p1}, p0_: {p0_}\""]],
		baseFn: (vals, done) => { console.log("Fn: ", vals.p0, vals.p1); done() },
		call: ["parameters.base a1 a2", "parameters.baseFn b1 b2"],
		asyncCall: [["parameters.base c1 c2", "parameters.base d1 d2"], "parameters.base e1 e2"],
		asyncCall2: [["parameters.asyncCall", "parameters.call"], "parameters.base f1 f2"],
		callFormatted0: "parameters.base {pkg.name} v1:{v1}",
		formatted: "parameters.base {p0}F {p1}f",
		callFormatted: "parameters.formatted {pkg.name} v1:{v1}"
	},
	cmd0: imp.cmds,
	build0: "echo this.build0",
	impTest: ["cmd0.build0", imp.cmds.build0, "cmd0.release.build0"],
	cmd1: require("./import").cmds,
	impTest1: ["cmd1.build0", "cmd1.release.build0"],
	release0: imp.cmds.release,
	cmd2: im.build0,
	replace: "replaceText readme.md tool test",
	replace2: "replaceText readme.md tool",
	replace3: "replaceText readme.md tool ''",
	replace4: "replaceText readme.md tool {v1}",
	copyMoveRemove: ["copy bin/ **/*.js tmp/js/", "move tmp tmp1/tmp2", "remove tmp1"],
	external: "ncmd -C ../b.web.spa build",
	test: ["tsc -p src", "node bin/test/utils.js"],
	cwd: {
		simple: ["cd src", process.platform == "win32" ? "dir" : "ls"],
		switch: [process.platform == "win32" ? "dir" : "ls",
			"cd src", process.platform == "win32" ? "dir" : "ls",
			"cd ../build", process.platform == "win32" ? "dir" : "ls"],
		_ref: process.platform == "win32" ? "dir" : "ls",
		ref: ["cd src", "cwd._ref"],
		_list: ["cd {p0}", process.platform == "win32" ? "dir" : "ls"],
		tool: ["cwd._list src", "cwd._list build", "cwd._list"],
	},
	dir: {
		simple: '../src: ' + (process.platform == "win32" ? "dir" : "ls"),
		list: '../src: ' + (process.platform == "win32" ? "dir" : "ls -l"),
		dash: '../node_modules/ts-node: ' + (process.platform == "win32" ? "dir" : "ls -l"),
		quotes: '"../node_modules/ts-node": ' + (process.platform == "win32" ? "dir" : "ls -l"),
	},
	cp: {
		file: "copy readme.md tmp",
		folder: "copy typings tmp",
		folders: "copy typings/**/mini* tmp",
		subFolders: "copy typings **/mini* tmp",
	},
	mv: {
		file: "move readme.md tmp",
		folder: "move typings tmp",
		folders: "move typings/mini* tmp",
		subFolders: "move typings mini* tmp",
	},
	ren: {
		file: "rename readme.md tmp",
		folder: "rename typings/shelljs tmp",
	},
	onError: {
		failing: "dir /{p0}",
		failing_error: "echo Test Error {p1}",
		subSub: "onError.failing {p1} {p0}",
		sub: "onError.subSub {p0} {p2}",
		test: "onError.sub param0 param1 param2",
	},
	ifCond: {
		listDir1: ['if isDir {p0}', "dir {p0}"],
		listDir2: ['if not isDir {p0}', "echo {p0} is missing"],
		listDir: ['ifCond.listDir1 {p0}', 'ifCond.listDir2 {p0}'],
		test: ["ifCond.listDir src", "ifCond.listDir notExistingFldr"],
	},
	show: {
		lines: "showLines readme.md '^#'",
	}
})
