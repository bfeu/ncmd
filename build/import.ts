
export var vals = {
		v01: "v01 test",
		v02: { v021: "v02.1 test" }
}

export var cmds = {
	build0: "echo \"build0\"",
	imp: 'echo "imported test"',
	release: {
		build0: ["echo \"release build0", "build0"]
	}
}
