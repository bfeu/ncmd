
exports.cmds = {
	test: {
		conf1: 'ncmd myConfig',
		conf2: require('./test/cmds').cmds.myConfig,
		conf3: ['cd test', 'ncmd myConfig'],
		env1: ['cd test', 'ncmd myVar'],
		// TODO: fix
		env2: require('./test/cmds').cmds.myVar,
		// TODO: fix
		env3: 'ncmd --cwd test myVar',
		// TODO: fix
		env4: 'ncmd myVar',
	},
}
