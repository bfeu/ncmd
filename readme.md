# ncmd

Node Command Line Tool.

## Install
> `npm --global install ncmd`

## How to Use
1) define your commands in a `cmds.js`
```javascript
exports.cmds = {
	go: 'echo "Go, go, gadgets!"',
	path: `echo ${process.env.PATH}`,
	serial: ['echo 1', 'echo 2'],
	parallel: [['echo 1', 'echo 2']],
	mixed: ['echo 1', ['echo 2a', 'echo 2b'], 'echo 3'],
}
```
Note: You can also use a `cmds.ts` with `export const cmds = ...` to benefit from auto-complete and type checking.

2) run your commands
> `ncmd go`

## Features
### Platform Specific
```javascript
exports.cmds = {
	env: process.platform == 'win32' ? 'set' : 'env',
}
```
### Scripts
```javascript
exports.cmds = {
	vals: (vals) => {
		console.log('--- all values ---')
		console.dir(vals)
	},
}
```
### Call Others
```javascript
exports.cmds = {
	go: 'echo "Go, go, gadgets!"',
	call: 'go',
}
```
or
```javascript
var someCmds = {
	go: 'echo "Go, go, gadgets!"',
}
exports.cmds = {
	call: someCmds.go,
}
```
### Local .bin Commands
Commands in `node_modules/.bin` are available via the PATH environment.
### Home Directory in Windows
You can use `~/somePersonal.txt` also on Windows.
### Config or .env Files
Handle your configuration with node require
```javascript
// config.js
exports.val = 'My config'

// cmds.js
exports.cmds = {
	myConfig: `echo ${require('./config.js').val}`,
}
```
or with a .evn file
```javascript
// .env
MY_VAR='My value'

// cmds.js
exports.cmds = {
	myVar: `echo ${process.env.MY_VAR}`,
}
```
The `.env` file is only valid for the current directory!
## File Layout
/build
Everything to build the tool.

/bin
The release files produced by the release build process.

/lib
External libraries and Typescript typings used by this tool.

/node_modules
Node.js modules to build and manage this tool.

/src
The source code files.

package.json
General information about this project.
