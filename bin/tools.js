"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
var fs = require("fs-extra");
var ncp = require("ncp");
var path = require("path");
var glob = require("glob");
var rimraf = require("rimraf");
var readline = require("readline");
var utils = require("./utils");
exports.tools = {
    copy: function (vals, done) { return copy(vals.p0, vals.p1, vals.p2, done); },
    link: function (vals, done) { return fs.ensureSymlink(vals.p0, vals.p1, done); },
    remove: function (vals, done) { return rimraf(vals.p0, done); },
    move: function (vals, done) { return move(vals.p0, vals.p1, vals.p2, done); },
    rename: function (vals, done) { return fs.rename(vals.p0, vals.p1, done); },
    mkdirs: function (vals, done) { return fs.mkdirs(vals.p0, done); },
    replaceText: function (vals, done) { return replaceText(vals.p0, vals.p1, vals.p2 || "", typeof vals.p3 === "string" ? vals.p3 : "gm", done); },
    createText: function (vals, done) { return fs.writeFile(vals.p0, vals.p1, {}, done); },
    prependText: function (vals, done) { return prependText(vals.p0, vals.p1, done); },
    appendText: function (vals, done) { return appendText(vals.p0, vals.p1, done); },
    readJson: function (vals, done, writableVals) { return readJson(vals.p0, vals.p1, writableVals, done); },
    readPackage: function (vals, done, writableVals) { return readPackage(vals.p0, vals.p1, writableVals, done); },
    showLines: function (vals, done) { return showLines(vals.p0, vals.p1, typeof vals.p2 === "string" ? vals.p2 : "", done); },
    vscodeTasks: function (vals, done) { return vscodeTasks(vals, done); },
    npmScripts: function (vals, done) { return npmScripts(vals, done); }
};
function processFiles(base, filesGlob, destPath, fn, opts, msg, done) {
    if (!destPath) {
        destPath = filesGlob;
        filesGlob = base;
        base = null;
    }
    if (base == ".")
        base = null;
    msg = filesGlob + " => " + msg + " ";
    if (base) {
        msg = base + " " + msg;
        filesGlob = path.join(base, filesGlob);
    }
    glob(filesGlob, { cwd: ".", nodir: false }, function (err, files) {
        if (err)
            return done(err);
        var fileCount = files.length;
        if (fileCount != 1 || files[0] != filesGlob)
            console.log(msg + fileCount + " files...");
        files.forEach(function (f) {
            var d = base ? f.replace(base, destPath) : path.join(destPath, f);
            fs.mkdirs(path.dirname(d), function (err) {
                if (err)
                    return done(err);
                fn(f, d, opts, function (err) {
                    if (err)
                        return done(err);
                    fileCount--;
                    if (fileCount <= 0)
                        done();
                });
            });
        });
    });
}
var moveOptions = { clobber: false, mkdirp: true };
var copyOptions = { clobber: true, perserveTimestamps: true };
function copy(base, filesGlob, destPath, done) {
    processFiles(base, filesGlob, destPath, ncp.ncp, copyOptions, "copying", done);
}
exports.copy = copy;
function move(base, filesGlob, destPath, done) {
    processFiles(base, filesGlob, destPath, fs.move, moveOptions, "moving", done);
}
exports.move = move;
// TODO: factor out common content toolling
function replaceText(filesGlob, regExp, replaceValue, flags, done) {
    glob(filesGlob, { cwd: "." }, function (err, files) {
        if (err)
            return done(err);
        var regEx = typeof regExp === "string" ? new RegExp(regExp, flags) : regExp;
        var fileCount = files.length;
        if (fileCount != 1 || files[0] != filesGlob)
            console.log(filesGlob + " => replacing content in " + fileCount + " files...");
        files.forEach(function (f) {
            console.log(f);
            fs.readFile(f, function (err, data) {
                if (err)
                    return done(err);
                var newContent = data.toString().replace(regEx, replaceValue);
                fs.writeFile(f, newContent, {}, function (err) {
                    if (err)
                        return done(err);
                    fileCount--;
                    if (fileCount <= 0)
                        done();
                });
            });
        });
    });
}
exports.replaceText = replaceText;
function showLines(filesGlob, filterRegExp, flags, done) {
    glob(filesGlob, { cwd: "." }, function (err, files) {
        if (err)
            return done(err);
        var regEx = typeof filterRegExp === "string" ? new RegExp(filterRegExp, flags) : filterRegExp;
        var fileCount = files.length;
        files.forEach(function (f) {
            var rl = readline.createInterface({ input: fs.createReadStream(f), terminal: false });
            rl.on('line', function (line) { if (!regEx || regEx.test(line))
                console.log(line); });
            rl.on('close', function () {
                if (err)
                    return done(err);
                fileCount--;
                if (fileCount <= 0)
                    done();
            });
        });
    });
}
exports.showLines = showLines;
function prependText(filesGlob, content, done) {
    glob(filesGlob, { cwd: "." }, function (err, files) {
        if (err)
            return done(err);
        var fileCount = files.length;
        if (fileCount != 1 || files[0] != filesGlob)
            console.log(filesGlob + " => prepending content in " + fileCount + " files...");
        files.forEach(function (f) {
            console.log(f);
            fs.readFile(f, function (err, data) {
                if (err)
                    return done(err);
                // TODO: directly via buffer?
                var newContent = content + data.toString();
                fs.writeFile(f, newContent, {}, function (err) {
                    if (err)
                        return done(err);
                    fileCount--;
                    if (fileCount <= 0)
                        done();
                });
            });
        });
    });
}
exports.prependText = prependText;
function appendText(filesGlob, content, done) {
    glob(filesGlob, { cwd: "." }, function (err, files) {
        if (err)
            return done(err);
        var fileCount = files.length;
        if (fileCount != 1 || files[0] != filesGlob)
            console.log(filesGlob + " => appending content in " + fileCount + " files...");
        files.forEach(function (f) {
            console.log(f);
            fs.readFile(f, function (err, data) {
                if (err)
                    return done(err);
                // TODO: directly via buffer?
                var newContent = data.toString() + content;
                fs.writeFile(f, newContent, {}, function (err) {
                    if (err)
                        return done(err);
                    fileCount--;
                    if (fileCount <= 0)
                        done();
                });
            });
        });
    });
}
exports.appendText = appendText;
function readJson(fileName, valKey, vals, done) {
    fs.readFile(fileName, function (err, data) {
        if (err)
            return done(err);
        try {
            vals[valKey] = JSON.parse(data.toString());
            done();
        }
        catch (err) {
            done(err);
        }
    });
}
exports.readJson = readJson;
function readPackage(fileName, valKey, vals, done) {
    fs.readFile(fileName, function (err, data) {
        if (err)
            return done(err);
        try {
            vals[valKey] = utils.formatPackage(JSON.parse(data.toString()));
            done();
        }
        catch (err) {
            done(err);
        }
    });
}
exports.readPackage = readPackage;
function vscodeTasks(vals, done) {
    var vsPath = vals.args.vscodePath || ".vscode";
    fs.mkdirsSync(vsPath);
    var tasksFile = path.join(vsPath, "tasks.json");
    var ncmdBin = "node_modules/ncmd/bin/ncmd";
    fs.readFile(tasksFile, function (err, data) {
        // expect comments within JSON
        var old = {};
        if (!err)
            eval("old = " + data);
        var json = old;
        json.command = "node";
        json.isShellCommand = true;
        var hasNcmdArg;
        if (!json.tasks)
            json.tasks = [];
        var tasks = {};
        if ("node" == old.command && old.tasks.length > 0) {
            // reuse valid old tasks and keep args
            for (var i = 0, t; (t = old.tasks[i]); ++i)
                tasks[t.taskName] = t;
            if (!json.args || json.args.length < 1) {
                hasNcmdArg = false;
            }
            else {
                var scriptArgs = json.args.filter(function (arg) { return arg.charAt(0) != '-'; });
                if (scriptArgs.length == 1 && /ncmd$/.test(scriptArgs[0])) {
                    hasNcmdArg = true;
                }
                else if (scriptArgs.length < 1) {
                    hasNcmdArg = false;
                }
                else {
                    // multiple node scripts are not supported here...
                    json.args = [ncmdBin];
                    hasNcmdArg = true;
                    json.tasks = [];
                    tasks = {};
                }
            }
        }
        else {
            json.args = [ncmdBin];
            hasNcmdArg = true;
        }
        var specs = utils.flatten(vals.cmds, {});
        for (var k in specs) {
            if (vals.args.exclude && k.search(vals.args.exclude) >= 0)
                continue;
            if (tasks[k])
                continue;
            var tsk = { "taskName": k, args: [] };
            if (!hasNcmdArg)
                tsk.args.push(ncmdBin);
            if ("ncmd.vscodeTasks" == k)
                tsk.args.push("--exclude", "^_|\\._");
            if ("ncmd.npmScripts" == k)
                tsk.args.push("--exclude", "^_|\\._");
            if (tsk.args.length < 1)
                delete tsk.args;
            json.tasks.push(tsk);
        }
        if (old.command != json.command
            || (old.args && old.args.length > 0 && old.args[0] != json.args[0])) {
            // stuff will be overwritten => save backup
            var f = "tasks_" + new Date().toISOString().replace(/:/g, '_') + ".json";
            fs.writeFile(path.join(vsPath, f), data, {}, function (err) {
                if (err)
                    // ignore for now...
                    console.log(err.message);
            });
        }
        fs.writeFile(tasksFile, JSON.stringify(json, null, '\t'), {}, done);
    });
}
exports.vscodeTasks = vscodeTasks;
function npmScripts(vals, done) {
    var json = __assign(__assign({}, vals.pkg), { versionMajor: void 0, versionMinor: void 0 });
    if (!json.scripts)
        json.scripts = {};
    var specs = utils.flatten(vals.cmds, {});
    for (var k in specs) {
        if (vals.args.exclude && k.search(vals.args.exclude) >= 0)
            continue;
        if (json.scripts[k])
            continue;
        json.scripts[k] = "ncmd " + k;
    }
    fs.writeFile("package.json", JSON.stringify(json, null, '\t'), {}, done);
}
exports.npmScripts = npmScripts;
//# sourceMappingURL=tools.js.map