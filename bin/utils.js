"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
var fs = require("fs");
function requireFirst(paths, verbose) {
    if (verbose === void 0) { verbose = true; }
    for (var i = 0, path; (path = paths[i]); ++i) {
        try {
            return { path: path, content: require(path) };
        }
        catch (err) {
            if (err.code !== "MODULE_NOT_FOUND")
                throw err;
            else if (verbose)
                console.error(err.message);
        }
    }
    return null;
}
exports.requireFirst = requireFirst;
function flatten(obj, objs, parentKey) {
    if (obj === void 0 || obj === null)
        return objs;
    Object.keys(obj).forEach(function (k) {
        var v = obj[k];
        var key = parentKey ? parentKey + '.' + k : k;
        var t = typeof v;
        if (t === 'string' || t === 'function' || Array.isArray(v)) {
            objs[key] = v;
            if (k === 'default' && parentKey)
                objs[parentKey] = v;
        }
        else
            flatten(v, objs, key);
    });
    return objs;
}
exports.flatten = flatten;
function formatObj(obj, vals) {
    if (typeof obj === "string")
        return format(obj, vals);
    var res = {};
    for (var k in obj)
        res[k] = formatObj(obj[k], vals);
    return res;
}
exports.formatObj = formatObj;
function format(str, vals, removeUnknown) {
    if (removeUnknown === void 0) { removeUnknown = false; }
    return str.replace(/\{([a-z_][^}]*)}/g, function (match, key) {
        var keys = key.split('.'), v = vals;
        for (var i = 0, k; (k = keys[i]) && v; ++i)
            v = v[k];
        return v || v == 0 ? v : removeUnknown ? "" : "{" + key + "}";
    });
}
exports.format = format;
function formatPackage(pkg) {
    return pkg && pkg.version ? __assign(__assign({}, pkg), { 
        // version parts
        versionMinor: pkg.version.substring(0, pkg.version.lastIndexOf('.')), versionMajor: pkg.version.substring(0, pkg.version.indexOf('.')) }) : pkg;
}
exports.formatPackage = formatPackage;
function extendEnvPath(env) {
    var paths = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        paths[_i - 1] = arguments[_i];
    }
    var PATH = "PATH";
    // windows calls it's path "Path" usually, but this is not guaranteed.
    if (process.platform === "win32") {
        PATH = "Path";
        Object.keys(env).forEach(function (e) { if (e.match(/^PATH$/i))
            PATH = e; });
    }
    var separator = process.platform === "win32" ? ";" : ":";
    var path = env[PATH] + separator || separator;
    for (var _a = 0, paths_1 = paths; _a < paths_1.length; _a++) {
        var p = paths_1[_a];
        if (path.indexOf(p + separator) < 0)
            path += p + separator;
    }
    env[PATH] = path.substr(0, path.length - 1);
    return env;
}
exports.extendEnvPath = extendEnvPath;
function fileStats(path) {
    try {
        return fs.statSync(path);
    }
    catch (err) {
        if (err.code !== "ENOENT")
            throw err;
    }
}
exports.fileStats = fileStats;
function parseArgs(argStr) {
    var q = null, s = 0, args = [];
    for (var i = 0; i < argStr.length; ++i) {
        var c = argStr.charAt(i);
        if (q && c != q)
            continue;
        else if (s == i && (c == ' '))
            s++;
        else if (c == ' ' || (q && c == q)) {
            args.push(argStr.substring(s, i));
            s = i + 1;
            q = null;
        }
        else if (s == i && (c == '"' || c == "'")) {
            q = c;
            s = i + 1;
        }
    }
    if (s < argStr.length && argStr.length > 0)
        args.push(argStr.substring(s));
    return args;
}
exports.parseArgs = parseArgs;
/** Mask an object with the members of another object (mask).
 * Array values of the mask replace all the values of the object member.
 */
function mask(obj, maskObj) {
    if (maskObj === void 0 || maskObj === null)
        return obj;
    if (obj === void 0 || obj === null)
        return maskObj;
    if (Array.isArray(obj))
        return Array.isArray(maskObj) ? __spreadArrays(maskObj) : [maskObj];
    if (typeof obj === 'object') {
        var res = {};
        for (var _i = 0, _a = Object.keys(obj); _i < _a.length; _i++) {
            var k = _a[_i];
            var v = obj[k];
            if (k in maskObj) {
                var m = maskObj[k];
                res[k] = mask(v, m);
            }
            else
                res[k] = v;
        }
        for (var _b = 0, _c = Object.keys(maskObj); _b < _c.length; _b++) {
            var k = _c[_b];
            if (!(k in obj))
                res[k] = maskObj[k];
        }
        return res;
    }
    return maskObj;
}
exports.mask = mask;
//# sourceMappingURL=utils.js.map